import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { ListModule } from './modules/list/list.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { ListPageComponent } from './modules/list/pages/list-page.component';
import { DetailsPageComponent } from './modules/details/pages/details-page.component';
import { DetailsModule } from './modules/details/details.module';

const appRoutes: Routes = [
  { path: 'list',                 component: ListPageComponent },
  { path: 'details/:countryCode', component: DetailsPageComponent},
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: '**', redirectTo: '/list' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ListModule,
    DetailsModule,
    SharedModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
