import { Component, OnInit } from '@angular/core';
import { GdpService } from 'src/app/shared/services/gpd/gdp.service';
import { CountryGdp } from '../../../shared/models/country-gdp';
import { ListSortOption } from '../list-sort-option';
import { ListSortDirection } from '../list-sort-direction';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  public freshCountryData: Array<CountryGdp> = [];
  public error = false;

  public sort: string = ListSortOption.COUNTRY_CODE;
  public direction: ListSortDirection = ListSortDirection.DESCENDING;

  constructor(
    private gdpService: GdpService
  ) {  }

  ngOnInit(): void {
    this.gdpService.getFreshGdpData$().subscribe(countryData => {
      this.freshCountryData.push(countryData);
    }, () => this.error = true);
  }

  handleSortChange(sortOption: ListSortOption) {
    if (sortOption === this.sort) {
      this.reverseOrder();
    } else {
      this.sortArray(sortOption);
    }
  }

  private sortArray(sortOption: ListSortOption): void {
    this.freshCountryData.sort((a, b) => {
      if (sortOption === ListSortOption.GDP) {
        return (a[sortOption] < b[sortOption]) ? 1 : -1;
      } else {
        return (a[sortOption] > b[sortOption]) ? 1 : -1;
      }
    });

    this.sort = sortOption;
    this.direction = ListSortDirection.DESCENDING;
  }

  private reverseOrder(): void {
    this.freshCountryData.reverse();

    this.direction = this.direction === ListSortDirection.DESCENDING ? ListSortDirection.ASCENDING : ListSortDirection.DESCENDING;
  }
}
