import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CountryGdp } from '../../../../shared/models/country-gdp';
import { ListSortOption } from '../../list-sort-option';
import { ListSortDirection } from '../../list-sort-direction';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent {
  @Input()
  public countryList: Array<CountryGdp>;

  @Input()
  public sorting: ListSortOption;

  @Input()
  public direction: ListSortDirection;

  public readonly sortOptions = ListSortOption;

  @Output()
  public sortChange = new EventEmitter<ListSortOption>();

  public handleTableHeadClick(sortOption: ListSortOption) {
    this.sortChange.emit(sortOption);
  }

  getClass(sortOption: ListSortOption) {
    return sortOption === this.sorting ? `active ${this.direction}` : '';
  }
}
