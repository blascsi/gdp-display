import { Component, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-country-list-item]',
  templateUrl: './country-list-item.component.html',
  styleUrls: ['./country-list-item.component.css']
})
export class CountryListItemComponent {
  @Input()
  public code: string;

  @Input()
  public name: string;

  @Input()
  public gdp: number;

  @Input()
  public date: Date;
}
