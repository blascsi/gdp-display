import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ListPageComponent } from './pages/list-page.component';
import { SharedModule } from '../../shared/shared.module';
import { CountryListComponent } from './components/country-list/country-list.component';
import { CountryListItemComponent } from './components/country-list-item/country-list-item.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    ListPageComponent,
    CountryListComponent,
    CountryListItemComponent
  ],
  exports: [
    ListPageComponent
  ]
})
export class ListModule { }
