export enum ListSortOption {
  COUNTRY_CODE = 'Country Code',
  COUNTRY_NAME = 'Country Name',
  YEAR = 'Year',
  GDP = 'Value'
}
