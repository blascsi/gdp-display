export enum ListSortDirection {
  ASCENDING = 'ascending',
  DESCENDING = 'descending'
}
