import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsChartComponent } from './components/details-chart/details-chart.component';
import { DetailsPageComponent } from './pages/details-page.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    DetailsPageComponent,
    DetailsChartComponent
  ],
  exports: [
    DetailsPageComponent
  ]
})
export class DetailsModule { }
