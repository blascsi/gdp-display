import { AfterViewInit, Component, ElementRef, Input, ViewChild, OnChanges } from '@angular/core';
import { Chart } from 'chart.js';

import { CountryGdp } from '../../../../shared/models/country-gdp';

@Component({
  selector: 'app-details-chart',
  templateUrl: './details-chart.component.html',
})
export class DetailsChartComponent implements AfterViewInit, OnChanges {
  @Input()
  public countryData: Array<CountryGdp>;

  @ViewChild('chartCanvas')
  private chartCanvas: ElementRef;

  private years = [];
  private gdpValues = [];
  private chart: Chart;

  ngAfterViewInit(): void {
    const ctx = this.chartCanvas.nativeElement.getContext('2d');

    this.chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.years,
        datasets: [
          {
            label: 'GDP Value',
            data: this.gdpValues,
            backgroundColor: 'rgba(51, 196, 240, .5)'
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        responsive: true,
        responsiveAnimationDuration: 0,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              callback: function (value) {
                if (parseInt(value, 10) >= 1000) {
                  return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                } else {
                  return '$' + value;
                }
              }
            }
          }]
        }
      }});
  }

  ngOnChanges(): void {
    if (!this.countryData || !this.chart) {
      return;
    }

    this.countryData.forEach(dataObj => {
      this.years.push(new Date(dataObj.Year).getFullYear().toString());
      this.gdpValues.push(dataObj.Value);
    });

    this.chart.update();
  }
}
