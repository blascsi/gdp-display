import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { GdpService } from '../../../shared/services/gpd/gdp.service';
import { Observable } from 'rxjs';
import { CountryGdp } from '../../../shared/models/country-gdp';

@Component({
  selector: 'app-details-page',
  templateUrl: './details-page.component.html',
  styleUrls: ['./details-page.component.css']
})
export class DetailsPageComponent implements OnInit {
  public countryData$: Observable<Array<CountryGdp>>;
  public countryName: string;
  public error = false;

  constructor(
    private route: ActivatedRoute,
    private gdpService: GdpService
  ) {
    this.countryData$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.gdpService.getGdpForCountryCode$(params.get('countryCode'))
      )
    );
  }

  ngOnInit() {
    this.countryData$.subscribe(
      countryData => this.countryName = countryData[0]['Country Name'],
      () => this.error = true);
  }
}
