export interface CountryGdp {
  'Country Code': string;
  'Country Name': string;
  'Value': number;
  'Year': string;
}
