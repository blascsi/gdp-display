import { TestBed } from '@angular/core/testing';

import { GdpService } from './gdp.service';

describe('GdpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GdpService = TestBed.get(GdpService);
    expect(service).toBeTruthy();
  });
});
