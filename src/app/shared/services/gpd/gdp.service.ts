import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  filter,
  flatMap,
  groupBy,
  map,
  mergeMap,
  toArray
} from 'rxjs/operators';

import { CountryGdp } from '../../models/country-gdp';

@Injectable({
  providedIn: 'root'
})
export class GdpService {
  private gdpUri =
    'https://pkgstore.datahub.io/90998f7f90e086bd5fc7c9075dfda43b/gdp/1/gdp_json/data/f11cb8c477556e91ca01e19619af0beb/gdp_json.json';
  private gdpData$: Observable<Array<CountryGdp>>;

  constructor(
    private http: HttpClient
  ) {
    this.gdpData$ = this.http.get<Array<CountryGdp>>(this.gdpUri);
  }


  /**
   * Returns an observable with only the newest GDP data for each country.
   */
  public getFreshGdpData$(): Observable<CountryGdp> {
    return this.getGroupedGdpList$().pipe(
      map(groupedList => {
        const sortedList = groupedList.sort(
          (a, b) => new Date(b.Year).getTime() - new Date(a.Year).getTime()
        );
        return sortedList[0];
      })
    );
  }

  /**
   * Returns an observable with all the GDP data for the country with a given country code.
   */
  public getGdpForCountryCode$(countryCode: string): Observable<Array<CountryGdp>> {
    return this.getGroupedGdpList$().pipe(
      filter(groupedList => groupedList[0]['Country Code'].toLowerCase() === countryCode.toLowerCase())
    );
  }

  /**
   * Used for grouping the raw GDP data, so we can process it more easily.
   * Returns multiple Arrays of CountryGdp elements,
   * where each array contains the data for one country.
   */
  private getGroupedGdpList$(): Observable<CountryGdp[]> {
    return this.gdpData$.pipe(
      flatMap(countryGdp => countryGdp),
      groupBy(countryGdp => countryGdp['Country Code']),
      mergeMap(group => group.pipe(toArray()))
    );
  }
}
