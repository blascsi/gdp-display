import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GdpService } from './services/gpd/gdp.service';
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { HeaderComponent } from './components/header/header.component';
import { ErrorIndicatorComponent } from './components/error-indicator/error-indicator.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    LoadingIndicatorComponent,
    HeaderComponent,
    ErrorIndicatorComponent
  ],
  exports: [
    LoadingIndicatorComponent,
    HeaderComponent,
    ErrorIndicatorComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        GdpService
      ]
    };
  }
}
